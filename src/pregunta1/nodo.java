package pregunta1;
/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva CI: 8537754
 * @param <T>
 */
public class nodo<T> {
    private T element;
    private nodo<T> next;
    
    public nodo(T element, nodo<T> next){
        this.element = element;
        this.next = next;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public nodo<T> getNext() {
        return next;
    }

    public void setNext(nodo<T> next) {
        this.next = next;
    }
}
