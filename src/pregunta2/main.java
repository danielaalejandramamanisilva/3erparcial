/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta2;

import java.util.Scanner;

/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva CI: 8537754
 */
public class main {
     public static void main(String[] args){
         Scanner z = new Scanner(System.in);
           lista<Integer> list = new lista<>();
           int continues = 0;
        while (continues == 0) {
            System.out.println("1.- Verificar si esta vacia: ");
            System.out.println("2.- Tamaño de la lista");
            System.out.println("3.- Recuperaar un valor especifico");
            System.out.println("4.- Recuperar el primer valor");
            System.out.println("5.- Recupera el ultimo valor ");
            System.out.println("6.- Agregar al ultimo");
            System.out.println("7.- Agregar al principio");
            System.out.println("8.- Agregar donde queramos");
            System.out.println("9.- Eliminar el primero");
            System.out.println("10.- Eliminar el ultimo");
            System.out.println("11.- Eliminar cualquiera");
            System.out.println("12.- Modificar un valor de la lista");
            System.out.println("13.- Ver el index de un nodo");
            System.out.println("14.- Mostrar los datos");

            System.out.println("Que desea hacer?");
            int optionSelected = z.nextInt();
            switch (optionSelected) {
                case 1:
                    System.out.println("la lista esta vacia: "+list.vacio());
                    break;
                case 2:
                    System.out.println("El tamaño de la lista es: "+list.tamañolista());
                    break;
                case 3:
                    System.out.println("ingrese el index del valor: ");
                    int dato=z.nextInt();
                    System.out.println("El valor es: "+list.sacar(dato));
                    break;
                case 4:
                    System.out.println("Recuperar el primer valor: "+list.sacarprimero());
                    break;
                case 5:
                    System.out.println("Recupera el ultimo valor: "+list.sacarultimo());
                    break;
                case 6:
                    System.out.println("Ingrese el dato");
                    int datou=z.nextInt();
                    System.out.println("agregar al ultimo: "+list.agregarultimo(datou));
                    break;
                case  7:
                    System.out.println("Ingrese el dato");
                    int datop=z.nextInt();
                    System.out.println("Agregar al principio: "+list.agregarprimero(datop));
                    break;
                case 8:
                    System.out.println("ingrese el valor y el index que desea agregar");
                    System.out.println("teniendo en cuenta que el index comienza en 0");
                    int index=z.nextInt();
                    int valor=z.nextInt();
                    System.out.println("Agregando el dato: "+list.agregarcualquierpos(valor, index));
                    break;
                case 9:
                    System.out.println("Eliminar el primero: "+list.eliminarprimero());
                case 10:
                    System.out.println("Eliminar el ultimo: "+list.eliminarultimo());
                    break;
                case 11:
                    System.out.println("ingrese el index del valor a eliminar: ");
                    int delete=z.nextInt();
                    System.out.println("Eliminando el dato: "+list.eliminarx(delete));
                    break;
                case 12:
                    System.out.println("ingrese el valor y el index del elemento a modificar");
                    System.out.println("teniendo en cuenta que el index comienza en 0");
                    int indexmod=z.nextInt();
                    int valormod=z.nextInt();
                    System.out.println("El dato modificado: "+list.modificar(valormod, indexmod));
                    break;
                case 13:
                    System.out.println("ingrese el index que desea ver ");
                    int indexlook=z.nextInt();
                    System.out.println("El index es : "+list.indexdeundato(indexlook));
                    break;
                case 14:
                    System.out.println("los datos son: "+list.contenidodelista());
                default:
                    System.out.println("----------");
                    break;
            }
            System.out.println("para conti presione 0 ");
            continues = z.nextInt();
        }
    }
}
